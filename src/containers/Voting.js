// import React, { useState } from 'react';
// import { Row, Col, Form, Button, FormGroup, Input, Label } from 'reactstrap';
// import alerts from 'sweetalert2';

// export default function VotingForm(props) {
//   const [overallRating, setOverallRating] = useState(0);
//   const [grease, setGrease] = useState(0);
//   const [cheese, setCheese] = useState(0);
//   const [breading, setBreading] = useState(0);

//   function handleSubmit(event) {
//     alerts.fire({
//       title: 'Am I working?',
//       text: 'Do you want to continue',
//       type: 'success',
//       confirmButtonText: 'Cool'
//     });
//     console.log(overallRating, grease, cheese, breading);
//   }

//   return (
//     <Form onSubmit={handleSubmit}>
//       <Row className="spacer">
//         <Col xs={{ size: 12 }}>
//           <FormGroup>
//             <Label>Overall Rating</Label>
//             <Input
//               type="select"
//               id="exampleSelect"
//               value={overallRating}
//               onChange={e => setOverallRating(e.target.value)}
//             >
//               <option value="0" disabled>
//                 Please select
//               </option>
//               <option value="1">1</option>
//               <option value="2">2</option>
//               <option value="3">3</option>
//               <option value="4">4</option>
//               <option value="5">5</option>
//             </Input>
//           </FormGroup>
//         </Col>
//       </Row>

//       <Row className="spacer">
//         <Col xs={{ size: 12 }}>
//           <FormGroup>
//             <Label for="exampleSelect">Grease</Label>
//             <Input
//               type="select"
//               name="select"
//               id="exampleSelect"
//               value={grease}
//               onChange={e => setGrease(e.target.value)}
//             >
//               <option value="0" disabled>
//                 Please select
//               </option>
//               <option value="1">1</option>
//               <option value="2">2</option>
//               <option value="3">3</option>
//               <option value="4">4</option>
//               <option value="5">5</option>
//             </Input>
//           </FormGroup>
//         </Col>
//       </Row>
//       <Row className="spacer">
//         <Col xs={{ size: 12 }}>
//           <FormGroup>
//             <Label for="exampleSelect">Cheese</Label>
//             <Input
//               type="select"
//               name="select"
//               id="exampleSelect"
//               value={cheese}
//               onChange={e => setCheese(e.target.value)}
//             >
//               <option value="0" disabled>
//                 Please select
//               </option>
//               <option value="1">1</option>
//               <option value="2">2</option>
//               <option value="3">3</option>
//               <option value="4">4</option>
//               <option value="5">5</option>
//             </Input>
//           </FormGroup>
//         </Col>
//       </Row>

//       <Row className="spacer">
//         <Col xs={{ size: 12 }}>
//           <FormGroup>
//             <Label for="exampleSelect">Breading</Label>
//             <Input
//               type="select"
//               name="select"
//               id="exampleSelect"
//               value={breading}
//               onChange={e => setBreading(e.target.value)}
//             >
//               <option value="0" disabled>
//                 Please select
//               </option>
//               <option value="1">1</option>
//               <option value="2">2</option>
//               <option value="3">3</option>
//               <option value="4">4</option>
//               <option value="5">5</option>
//             </Input>
//           </FormGroup>
//         </Col>
//       </Row>

//       <Row className="spacer">
//         <Col xs={{ size: 12 }}>
//           <Button onClick={handleSubmit}>Submit</Button>
//         </Col>
//       </Row>
//     </Form>
//   );
// }
