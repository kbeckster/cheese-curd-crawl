import React, { useState } from 'react';
import {
  Row,
  Col,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Form,
  Button,
  FormGroup,
  Input,
  Label
} from 'reactstrap';
import alerts from 'sweetalert2';

export default function LocationSelection(props) {
  const [overallRating, setOverallRating] = useState('');
  const [grease, setGrease] = useState('');
  const [cheese, setCheese] = useState('');
  const [breading, setBreading] = useState('');
  const [dropdownState, setdropdownState] = useState(false);
  const [location, setLocation] = useState('');
  const [locationText, setLocationText] = useState('Select a Stop');
  const [user, setUser] = useState('');
  const [showResults, setShowResults] = useState(false);

  function handleSubmit(event) {
    if (
      !overallRating ||
      !grease ||
      !cheese ||
      !breading ||
      !location ||
      !user
    ) {
      alerts.fire({
        title: 'Error!',
        text: 'Must fill out all items on form before submitting',
        type: 'error',
        confirmButtonText: 'Okay'
      });
    } else {
      let data = {
        user_name_text: user,
        location_text: location,
        grease: grease,
        cheese: cheese,
        breading: breading,
        overall_rating: overallRating
      };

      fetch(
        'https://9f7ac6z7zd.execute-api.us-east-1.amazonaws.com/post-rating/ratings',
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(data)
        }
      )
        .then(res => res.json())
        .then(data => console.log(data))
        .catch(err => console.log(err));

      alerts.fire({
        title: 'Cheesy!',
        type: 'success',
        confirmButtonText: 'Okay'
      });
    }
  }

  function toggle() {
    setdropdownState(dropdownState === false ? true : false);
  }

  function handleChange(e) {
    setLocation(e.target.value);
    setLocationText(e.target.innerText);
  }

  function getResults() {
    console.log('I will get results');
  }

  return (
    <Col xs={{ size: 12 }}>
      <Form>
        <FormGroup>
          <Label for="username">Name</Label>
          <Input
            type="text"
            name="user"
            id="user-name"
            value={user}
            placeholder="Enter name here"
            onChange={e => setUser(e.target.value)}
          />
        </FormGroup>
        <Row className="spacer">
          <Col xs={{ size: 12 }}>
            <FormGroup>
              <ButtonDropdown
                isOpen={dropdownState}
                toggle={toggle}
                onChange={handleChange}
                value={location}
              >
                <DropdownToggle
                  caret
                  color="secondary"
                  text="hello"
                  onChange={handleChange}
                >
                  {locationText}
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem value="mke" onClick={handleChange}>
                    Milwaukee Brewing Company
                  </DropdownItem>
                  <DropdownItem value="rumpus" onClick={handleChange}>
                    The Rumpus Room
                  </DropdownItem>
                  <DropdownItem value="harley" onClick={handleChange}>
                    Harley-Davidson Museum
                  </DropdownItem>
                  <DropdownItem value="fuel" onClick={handleChange}>
                    Fuel Cafe 5th St
                  </DropdownItem>
                  <DropdownItem value="sheep" onClick={handleChange}>
                    Black Sheep MKE
                  </DropdownItem>
                  <DropdownItem value="outsider" onClick={handleChange}>
                    The Outsider
                  </DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
            </FormGroup>
          </Col>
        </Row>

        <Row className="spacer">
          <Col xs={{ size: 12 }}>
            <FormGroup>
              <Label>Overall Rating</Label>
              <Input
                type="select"
                id="exampleSelect"
                value={overallRating}
                onChange={e => setOverallRating(e.target.value)}
              >
                <option value="" disabled>
                  Please select
                </option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
              </Input>
            </FormGroup>
          </Col>
        </Row>

        <Row className="spacer">
          <Col xs={{ size: 12 }}>
            <FormGroup>
              <Label for="exampleSelect">Grease</Label>
              <Input
                type="select"
                name="select"
                id="exampleSelect"
                value={grease}
                onChange={e => setGrease(e.target.value)}
              >
                <option value="" disabled>
                  Please select
                </option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
              </Input>
            </FormGroup>
          </Col>
        </Row>
        <Row className="spacer">
          <Col xs={{ size: 12 }}>
            <FormGroup>
              <Label for="exampleSelect">Cheese</Label>
              <Input
                type="select"
                name="select"
                id="exampleSelect"
                value={cheese}
                onChange={e => setCheese(e.target.value)}
              >
                <option value="" disabled>
                  Please select
                </option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
              </Input>
            </FormGroup>
          </Col>
        </Row>

        <Row className="spacer">
          <Col xs={{ size: 12 }}>
            <FormGroup>
              <Label for="exampleSelect">Breading</Label>
              <Input
                type="select"
                name="select"
                id="exampleSelect"
                value={breading}
                onChange={e => setBreading(e.target.value)}
              >
                <option value="" disabled>
                  Please select
                </option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
              </Input>
            </FormGroup>
          </Col>
        </Row>

        <Row className="spacer">
          <Col xs={{ size: 3, offset: 3 }}>
            <Button color="info" onClick={handleSubmit}>
              Submit
            </Button>
          </Col>
          <Col xs={{ size: 3 }}>
            <Button outline color="info" size="xs" href="/results">
              Results
            </Button>
          </Col>
        </Row>
      </Form>
    </Col>
  );
}
