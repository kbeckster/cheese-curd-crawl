import React, { useState } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';

export default function Results(props) {
  return (
    <Container>
      <Row className="spacer">
        <Col sm="12" md={{ size: 6 }}>
          <h5>
            Results will post here after the crawl. Aka Becky did not have time
            to get to this :)
          </h5>
        </Col>
      </Row>

      <Row className="spacer">
        <Col sm="12" md={{ size: 6 }}>
          <Button outline color="info" size="xs" href="/">
            Back
          </Button>
        </Col>
      </Row>
    </Container>
  );
}
