import React, { Component } from 'react';
import { Switch, Route, Link } from 'react-router-dom';
import { Container, Row, Col, Button } from 'reactstrap';
import LocationSelection from './containers/Main';
import Results from './containers/Results';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Container className="spacer">
          <Row>
            <Col sm="12" md={{ size: 6 }}>
              <h3>Cheese Curd Crawl 2019 </h3>
            </Col>
          </Row>
          <Row className="spacer">
            <Col>
              <Switch>
                <Route exact path="/" component={LocationSelection} />
                {/* both /roster and /roster/:number begin with /roster */}
                <Route path="/results" component={Results} />
              </Switch>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default App;
